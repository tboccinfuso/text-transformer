import "package:test/test.dart";
import 'package:text_transformer/src/format.dart';

void main() {
  group('String Format |', () {
    test('Trim() default trim padding on both sides', () {
      const String str = ' hello world ';

      expect(Trim(str), matches('hello world'));
    });

    test('Trim() right side', () {
      const String str = 'hello world ';

      expect(Trim(str, 'right'), matches('hello world'));
    });

    test('Trim() left side', () {
      const String str = ' hello world';

      expect(Trim(str, 'left'), matches('hello world'));
    });

    test('Trim() invalid <side> passed', () {
      const String str = ' hello world ';

      expect(() => Trim(str, 'middle'), throwsA(TypeMatcher<FormatException>()));
    });
  });
}