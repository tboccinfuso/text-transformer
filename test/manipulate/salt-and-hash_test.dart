import "package:test/test.dart";
import 'package:text_transformer/src/manipulate.dart';

void main() {
  group('String Manipulation | SaltAndHash', () {

    group('Defaults |', () {
      test('SaltAndHash() returns with salt<String>', () {
        const String str = 'hello';

        expect(SaltAndHash(str)['salt'], TypeMatcher<String>());
      });

      test('SaltAndHash() returns with hash<String>', () {
        const String str = 'hello';

        expect(SaltAndHash(str)['hash'], TypeMatcher<String>());
      });

      test('hash ends up with 64 char length (sha256)', () {
        const String str = 'hello';

        expect(SaltAndHash(str)['hash'], hasLength(64));
      });
    });

    group('Optional Params |', () {
      test('md5 hash ends up with 32 char length', () {
        const String str = 'hello';

        expect(SaltAndHash(str, 'md5')['hash'], hasLength(32));
      });

      test('sha224 hash ends up with 32 char length', () {
        const String str = 'hello';

        expect(SaltAndHash(str, 'sha224')['hash'], hasLength(56));
      });

      test('sha384 hash ends up with 96 char length', () {
        const String str = 'hello';

        expect(SaltAndHash(str, 'sha384')['hash'], hasLength(96));
      });

      test('sha1 hash ends up with 40 char length', () {
        const String str = 'hello';

        expect(SaltAndHash(str, 'sha1')['hash'], hasLength(40));
      });
    });

  });
}