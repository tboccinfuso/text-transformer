import "package:test/test.dart";
import 'package:text_transformer/src/manipulate.dart';

void main() {
  group('String Manipulation | Truncate', () {
    const _text = 'This is some text that I want to truncate please!';
    test('truncate text at 30 chars', () {
      expect(Truncate(_text), hasLength(32));
    });

    test('truncate text at 13 chars', () {
      expect(Truncate(_text, 10), hasLength(13));
    });
  });
}