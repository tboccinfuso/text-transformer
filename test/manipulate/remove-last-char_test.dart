import "package:test/test.dart";
import 'package:text_transformer/src/manipulate.dart';

void main() {
  group('String Manipulation | RemoveLastChar', () {
    test('Defaults', () {
      const String str = 'helloX';
      expect(RemoveLastChar(str), matches('hello'));
    });
  });
}