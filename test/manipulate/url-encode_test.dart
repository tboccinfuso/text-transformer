import "package:test/test.dart";
import 'package:text_transformer/src/manipulate.dart';

void main() {
  group('String Manipulation | UrlEncode', () {
    test('url encode a string', () {
      expect(UrlEncode('hello world'), matches('hello%20world'));
    });
  });

  group('String Manipulation | UrlEncodeFromMap', () {
    test('encode Map', () {
      expect(UrlEncodeFromMap({ 'test': 123, 'arr': [123, 'string'] }), matches('test=123&arr=%5B123%2C%20string%5D'));
    });
  });

  group('String Manipulation | UrlDecode', () {
    test('decodes url data', () {
      expect(UrlDecode('.com?arr=%5B123%2C%20string%5D'), contains('arr=[123, string]'));
    });
  });

  group('String Manipulation | UrlDecodeQueryString', () {
    test('Returns type Map', () {
      expect(URlDecodeQueryString('.com?num=123&arr=%5B123%2C%20string%5D'), TypeMatcher<Map>());
    });

    test('Parses query param', () {
      expect(URlDecodeQueryString('.com?num=123&arr=%5B123%2C%20string%5D')['num'], matches('123'));
    });

    test('Invalid url passed returns empty Map', () {
      expect(URlDecodeQueryString('hello world'), isEmpty);
    });
  });
}