import "package:test/test.dart";
import 'package:text_transformer/src/manipulate.dart';

void main() {
  group('String Manipulation | StripHTML', () {
    test('replace html tags with nothing', () {
      expect(StripHTML('<h1>Title</h1>'), equals('Title'));
      expect(StripHTML('Inject a <script src="asdasd"></script> script tag.'), equals('Inject a script tag.'));
    });
  });
}