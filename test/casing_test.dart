import "package:test/test.dart";
import 'package:text_transformer/src/casing.dart';

void main() {
  group('Changing Case |', () {
    test('UpperCaseFirstLetter() will capitalize the first letter in a string', () {
      const String str = 'hello world';

      expect(UpperCaseFirstLetter(str), matches('Hello world'));
    });

    test('UpperCaseFirstLetter() will capitalize each word in the string', () {
      const String str = 'hello world';

      expect(UpperCaseFirstLetter(str, true), matches('Hello World'));
    });
  });
}