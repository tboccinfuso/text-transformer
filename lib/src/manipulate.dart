import 'dart:math';

import 'package:crypto/crypto.dart';
import 'dart:convert';

/// Return a Map containing the salt & hashed string
///
/// [str] String to hash
///
/// [format] String/optional - hashing algorithm to use. [options are: 'sha256', 'sha224', 'sha384', 'sha1', 'md5']
///
/// Example:
/// ```dart
///   SaltAndHash('hello', 20, 'md5'); // {salt: 7Uj1PjgI6aGPTNgGiUh3myFuv0c=, hash: 8a96e82fe4d32e6f23822aea445289c115216e21a6f9ebe2281b90cb751fd1a3}
/// ```
Map<String, String> SaltAndHash(String str, [String format = 'sha256']) {
  // Salt gen
  var _random = Random.secure();
  var _values = List<int>.generate(20, (i) => _random.nextInt(256));

  var _salt = base64Url.encode(_values);
  var _bytes = utf8.encode(_salt + str);

  return {'salt': _salt, 'hash': _saltAndHashHelper(format, _bytes)};
}

// Helper to ensure the algorithm + return format string
String _saltAndHashHelper(String format, List<int> bytes) {
  const List<String> _algorithms = [
    'sha256',
    'sha224',
    'sha384',
    'sha1',
    'md5',
  ];
  String _returnString = '';

  if (!_algorithms.contains(format)) {
    throw new FormatException(
        "$format does not match a valid option: $_algorithms");
  }

  switch (format) {
    case 'md5':
      _returnString = md5.convert(bytes).toString();
      break;
    case 'sha1':
      _returnString = sha1.convert(bytes).toString();
      break;
    case 'sha384':
      _returnString = sha384.convert(bytes).toString();
      break;
    case 'sha224':
      _returnString = sha224.convert(bytes).toString();
      break;
    default:
      _returnString = sha256.convert(bytes).toString();
  }

  return _returnString;
}

/// Removes the last character in a string
///
/// [str] String
String RemoveLastChar(String str) {
  return str.substring(0, str.length - 1);
}

/// Url encode a String
///
/// [str] String
String UrlEncode(String str) {
  return Uri.encodeFull(str);
}

/// Decode a url
///
/// [url] String
///
/// Example:
/// ```dart
///   UrlDecode('test=123&arr=%5B123%2C%20string%5D') // test=123&arr=[123, string]
/// ```
String UrlDecode(String url) {
  return Uri.decodeFull(url);
}

/// Decode a query string ( or full Url) and return a Map
///
/// [url] String
///
/// Example:
/// ```dart
/// URlDecodeQueryString('.com?num=123&arr=%5B123%2C%20string%5D'); // {num: 123, arr: [123, string]}
/// ```
Map<String, dynamic> URlDecodeQueryString(String url) {
  if (!url.contains(RegExp(r'\?(?:&?[^=&]*=[^=&]*)*$'))) {
    print("url does not have a valid query string.");
    return {};
  }

  final _decodedUrl = Uri.decodeFull(url).split('?');
  final _queryString = _decodedUrl[1];

  if (_queryString.contains('&')) {
    Map<String, dynamic> _map = new Map();

    for (var _q in _queryString.split('&')) {
      _map[_q.split('=')[0].toString()] = _q.split('=')[1];
    }

    return _map;
  }

  return {
    _queryString.split('=')[0].toString() : _queryString.split('=')[1]
  };
}

/// Url encode a Map
///
/// [data] Map
///
/// Example:
/// ```dart
///   UrlEncodeFromMap({ 'test': 123, 'arr': [123, 'string'] }) // test=123&arr=%5B123%2C%20string%5D
/// ```
String UrlEncodeFromMap(Map data) {
  return data.keys.map((key) => "${Uri.encodeComponent(key)}=${Uri.encodeComponent(data[key].toString())}").join("&");
}

/// Truncate and add ellipsis. Trims off trailing spaces.
///
/// [str] String
String Truncate(String str, [int length = 30]) {
  return str.substring(0, length).trim() + '...';
}

/// Removes all HTML tags from a string and replaces them with nothing.
///
/// [str] String
String StripHTML(String str) {
  String _cleaned = str.replaceAll(RegExp("<(.|\n)*?>"), '');

  return _cleaned.replaceAll('  ', ' ');
}