/// Wrapper for trim(), trimRight(), trimLeft(). Defaults to [trim()] if no [side] is passed.
///
/// [str] String to trim
///
/// [side] String/Optional - what side to trim on
///
/// Example:
/// ```dart
/// Trim(' hello', 'left'); // 'hello'
/// Trim('hello ', 'right'); // 'hello'
/// Trim(' hello '); // 'hello'
/// ```
String Trim(String str, [String side]) {
  const _options = ['left', 'right'];

  // Throw error is not a matching side is passed
  if (side != null && !_options.contains(side)) {
    throw FormatException(
        "$side does not match one of the following values: $_options");
  }

  if (side == 'left') {
    return str.trimLeft();
  } else if (side == 'right') {
    return str.trimRight();
  }

  // default to trim both sides
  return str.trim();
}
