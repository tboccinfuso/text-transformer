import 'package:text_transformer/src/format.dart';

/// Uppercase the first letter of a string
///
/// [str] <String> - required
///
/// [everyWord] <bool/optional> - pass true if you want every word in the string to be UCFL
String UpperCaseFirstLetter(String str, [bool everyWord = false]) {
  if (everyWord) {
    List<String> _words = str.split(' ');
    String _return = '';

    for (String _word in _words) {
      _return += _word.substring(0, 1).toUpperCase() + _word.substring(1, _word.length) + ' ';
    }

    return Trim(_return, 'right');
  }

  return str.substring(0, 1).toUpperCase() + str.substring(1, str.length);
}
